# Yura Harlamov

site: <https://harlamov.org>  
repo: <https://gitlab.com/harlamov>  
telegram: <https://t.me/harlamov_yu>

## Career Overview

I'm a linux engineer with more than 10 years experience in the IT area. I specialize in linux systems, highload web-projects, DevOps (Kubernetes, Gitlab, Docker). Also works with huge installations of various hardware: servers, blade systems, storages, network devices. Experience in monitoring, alerting and data collecting tools (Prometheus + Grafana, EFK/ELK, Loki, etc). Experience in administration and scripting configuration management tools (Puppet, Terraform, Ansible).

## Work Experience

xsolla.com: 2 years  
fact.digital: 3 years  
Other companies: 7 years  
Total: 12 yrs 6 months

### xsolla.com

DevOps engineer. IT Department.  
Kuala-Lumpur, Malaysia  
April 2022 — now  

#### Responsibilities and duties

- Support and creation of different project environments.
- Working on infrastructure automation via Puppet and Terraform.
- Monitoring the status of services and prompt solution of emerging problems.
- Support for multiple servers on Linux.

#### Achievements

- Implemented NetBox for inventory and IP address management.  
- Implemented Terraform for managing VMvare
- Improve monitoring with Prometheus and Grafana
- Improve Onduty

### fact.digital

Lead Linux administrator. IT Department.  
Magnitogorsk, Russia  
April 2019 — 2022 April

#### Responsibilities and duties

- Participation in the development and operation of high-load projects.
- Automation of the creation of test and production environments.
- Server and application monitoring.
- Troubleshooting using htop, lsof, tcpdump, strace, etc.
- Incident response.
- Working on infrastructure automation via Ansible.
- Onboarding and coaching new members of the team.
- Optimization business processe

#### Achievements

- Reduced test environment creation time from 1 hour to 3 minutes.
- Introduced CI/CD into the software development process.
- Launched several projects for Enterprise clients in the field of metallurgy, gold mining, jewelry and other

### LLC Office Telecom

Director. Co-founder.  
Magnitogorsk, Russia  
December 2015 — 2019 August  

#### Responsibilities and duties

- Negotiations with clients and partners.  
- Organization of work at customer sites.  
- Purchase of inventory and tools.  
- Direct participation in the implementation of projects for clients.  

#### Achievements

- Implemented several IP PBX projects.  
- A network of more than 10 km of cable was built.  
- A network of commercial TVs has been built and remote download and display of advertisements using open source software has been established.

### LLC Uzhuralmost

System Administrator. IT Department.  
Magnitogorsk, Russia  
January 2012 — 2019 April  

#### Responsibilities and duties

- User support.  
- Network building.  
- Equipment inventory.  
- Negotiations with equipment suppliers and internet providers.  

#### Achievements

- Replaced analog PBX with IP PBX.  
- Reduced telephone costs.  
- Implemented ProxMox.  
- Fiber optic networks built.  

## Education

Graduation year: 2016  
University: Nosov Magnitogorsk State Technical University  
Specialty: Applied Informatics  

Graduation year: 2011  
University: Kuban State University  
Specialty: Software of automated systems  

### Courses

[Prometheus Certified Associate](certificates/prometheus.pdf)  
[GitLab Certified CI/CD Associate](certificates/iurii-kharlamov-3deb646a-4d9b-4533-99f0-d50363d2d30d-certificate.pdf)  
[Linux Basics Course & Labs](certificates/Linux-Basics-Course-Labs.pdf)  
[Linux Challenges](certificates/Linux-Challenges.pdf)  
[Linux implementation](certificates/Linux-implementation.pdf)  
[Kubernetes for users](certificates/Kubernetes-for-users.pdf)  
[Terraform Basic](certificates/Terraform-Basic.pdf)  
[Helm](certificates/Helm-for-Beginners.pdf)  
[Network](certificates/Network.pdf)  
[Kanban](certificates/Kanban.pdf)  
[Behavioral Report](certificates/Behavioral-Report.pdf)  

### Skills

> Centos/Ubuntu, GitLab CI, Docker, K8s, Nginx, Apache, Php-fpm, Asterisk,  
> Jira, Confluence, Miro,  
> ELK, Zabbix, Prometheus, Grafana,  
> Puppet, Ansible, Terraform, Bash, Golang,  
> VMware, oVirt, ProxMox, GCP.

<img src=".badge/pca-prometheus-certified-associate.png" alt="Prometheus Certified Associate" width="100" height="100">
<img src=".badge/uni-credential-emblem-jirafundamentals.png" alt="Atlassian University" width="100" height="100">
<img src=".badge/gitlab-ci-cd.png" alt="GitLab Certified CI/CD Associate" width="100" height="100">
